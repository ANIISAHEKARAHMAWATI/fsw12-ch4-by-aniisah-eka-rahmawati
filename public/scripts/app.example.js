class App {
  constructor() {
    this.carContainerElement = document.getElementById('cars-container')
  }

  async init() {
    await this.load()
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement('div')
      node.classList += 'col-md-4'
      node.innerHTML = car.render()
      this.carContainerElement.appendChild(node)
    })
  }

  async load() {
    this.clear()
    var tipeDriverInput = document.getElementById('tipe-driver')
    var tanggalInput = document.getElementById('tanggal')
    var jemputInput = document.getElementById('jemput')
    var jumlahPenumpangInput = document.getElementById('penumpang')

    const cars = await Binar.listCars()
    const carsFilter = cars.filter((car) => {
      var tipeDriver = tipeDriverInput.value == '1'
      var tanggal = tanggalInput.value
      var jemput = jemputInput.value
      var tanggalJemput = new Date(tanggal + ' ' + jemput)
      var cekAvailable = tanggalJemput.getTime() - car.availableAt.getTime()
      var penumpang = jumlahPenumpangInput.value
      if (tanggal) {
        if (penumpang)
          return (
            tipeDriver == car.available &&
            cekAvailable > 0 &&
            car.capacity >= parseInt(penumpang)
          )
        return cekAvailable > 0 && tipeDriver == car.available
      }

      return car
    })
    Car.init(carsFilter)
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild

    while (child) {
      child.remove()
      child = this.carContainerElement.firstElementChild
    }
  }
}
